<?php

namespace App\Repository;

use App\Entity\Posts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class PostsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Posts::class);
    }
    public function findByCompte($facebook,$twitter,$limit) {

                  $qb= $this->createQueryBuilder('p');

                  $qb-> join('p.comptes','c')

                  ->andWhere('c.nom LIKE :facebook OR c.nom LIKE :twitter ')
                  ->setParameter('facebook', '%'.$facebook.'%')
                  ->setParameter('twitter', '%'.$twitter.'%')
                  ->orderBy('p.dateCreation', 'DESC')
                  ->setMaxResults($limit);
                  return $qb->getQuery()->getResult() ;



              ;



}

public function findByOtherCompte($autre,$limit) {

              $qb= $this->createQueryBuilder('p');


              $qb -> join('p.comptes','c')

              ->andWhere('c.nom LIKE :autre')

              ->setParameter('autre', '%'.$autre.'%')
              ->orderBy('p.dateCreation', 'DESC')

              ->setMaxResults($limit);
              return $qb->getQuery()->getResult() ;



}
}
