<?php

namespace App\Repository;

use App\Entity\Works;
use App\Entity\Tags;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class WorksRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Works::class);
    }

public function findByTag(int $id){
  $conn = $this->getEntityManager()->getConnection();

     $sql = '
     SELECT * FROM works w

     JOIN works_has_tags ON w.id = works

     JOIN tags ON tags.id = works_has_tags.tags
     WHERE works_has_tags.tags IN (select works_has_tags.tags from works_has_tags where works_has_tags.works = :id)




         ';
     $stmt = $conn->prepare($sql);
     $stmt->execute(['id' => $id]);

     // returns an array of arrays (i.e. a raw data set)
     return $stmt->fetchAll();

}
}
