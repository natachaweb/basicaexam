<?php
/*
  ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tags;
use Symfony\Component\HttpFoundation\Request;

class TagsController extends GenericController {

    public function showAction(int $id, Request $request){
      /*$posts = [];
          if ($page->getId() === 3) {
            $posts = $this->getDoctrine()
                          ->getRepository(\App\Entity\Post::class)
                          ->findBy([], [], 10);
          }
      */
      $tag = $this->_repository->find($id);
      return $this->render('tags/show.html.twig',[
        'tag'   => $tag,

        /*'posts' => $posts */
      ]);
    }

    public function indexAction(Request $request){




      $tags = $this->_repository->findAll();
      return $this->render('tags/index.html.twig',[
        'tags' => $tags,


      ]);
    }

}
