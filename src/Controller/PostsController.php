<?php
/*
  ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Posts;
use Symfony\Component\HttpFoundation\Request;

class PostsController extends GenericController {

    public function showAction(int $id, Request $request){
      /*$posts = [];
          if ($page->getId() === 3) {
            $posts = $this->getDoctrine()
                          ->getRepository(\App\Entity\Post::class)
                          ->findBy([], [], 10);
          }
      */
      $post = $this->_repository->find($id);
      return $this->render('posts/show.html.twig',[
        'post'   => $post,

        /*'posts' => $posts */
      ]);
    }

    public function indexAction($vue='liste',int $limit = null){



      $postsReseauxSociaux = $this->_repository->findByCompte($facebook='facebook',$twitter='twitter',$limit);
      $posts = $this->_repository->findByOtherCompte($autre='autre',$limit);
      return $this->render('posts/'.$vue.'.html.twig',[
        'posts' => $posts,
        'postsReseauxSociaux' => $postsReseauxSociaux

      ]);
    }

}
