<?php
/*
  ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Works;

use Symfony\Component\HttpFoundation\Request;

class WorksController extends GenericController {

    public function showAction(int $id, Request $request){
      /*$posts = [];
          if ($page->getId() === 3) {
            $posts = $this->getDoctrine()
                          ->getRepository(\App\Entity\Post::class)
                          ->findBy([], [], 10);
          }
      */

      $work = $this->_repository->find($id);
      $simularWorks =  $this->_repository->findByTag($id);
      return $this->render('works/show.html.twig',[
        'work'   => $work,
        'simularWorks'=> $simularWorks

        /*'posts' => $posts */
      ]);
    }

    public function indexAction( $vue='index',array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null){


      $works = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('works/'.$vue.'.html.twig',[
        'works' => $works,


      ]);

    }

    public function sliderAction($vue='liste'){




      $works = $this->_repository->findAll();
      return $this->render('works/'.$vue.'.html.twig',[
        'works' => $works,


      ]);

    }

}
