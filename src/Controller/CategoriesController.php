<?php
/*
  ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Categories;
use Symfony\Component\HttpFoundation\Request;

class CategoriesController extends GenericController {

    public function showAction(int $id, Request $request){
      /*$posts = [];
          if ($page->getId() === 3) {
            $posts = $this->getDoctrine()
                          ->getRepository(\App\Entity\Post::class)
                          ->findBy([], [], 10);
          }
      */
      $categorie = $this->_repository->find($id);
      return $this->render('categories/show.html.twig',[
        'categorie'   => $categorie,

        /*'posts' => $posts */
      ]);
    }

    public function indexAction(Request $request){




      $categories = $this->_repository->findAll();
      return $this->render('categories/index.html.twig',[
        'categories' => $categories,


      ]);
    }

}
