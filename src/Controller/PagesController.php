<?php
/*
  ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Pages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class PagesController extends GenericController {

    public function showAction(int $id, Request $request){
      /*$posts = [];
          if ($page->getId() === 3) {
            $posts = $this->getDoctrine()
                          ->getRepository(\App\Entity\Post::class)
                          ->findBy([], [], 10);
          }
      */
      $page = $this->_repository->find($id);
      return $this->render('pages/show.html.twig',[
        'page'   => $page,

        /*'posts' => $posts */
      ]);
    }

    public function indexAction(Request $request){







      $pages = $this->_repository->findAll();
      return $this->render('pages/index.html.twig',[
        'pages' => $pages,
    

      ]);
    }

}
